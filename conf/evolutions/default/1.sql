# --- Created by Ebean DDL
# To stop Ebean DDL generation, remove this comment and start using Evolutions

# --- !Ups

create table news (
  id                            bigint auto_increment not null,
  title                         varchar(255) not null,
  subtitle                      varchar(255),
  image                         varchar(255),
  news_date                     datetime(6),
  constraint pk_news primary key (id)
);


# --- !Downs

drop table if exists news;

