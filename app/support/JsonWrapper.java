package support;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.util.List;

/**
 * Created by idgre on 11.06.2016.
 */
@Singleton
public class JsonWrapper<T> {

    private ObjectMapper mapper;

    @Inject
    public JsonWrapper(ObjectMapper mapper) {
        this.mapper = mapper;
    }

    public String wrapResponse(List<T> objects, String container, int total) {
        JsonNode objectsNode = mapper.valueToTree(objects);
        ObjectNode wrappedResponse = mapper.createObjectNode();
        wrappedResponse.put("total", total);
        wrappedResponse.set(container, objectsNode);
        return wrappedResponse.toString();
    }

    public String wrapError(Exception e, JsonNode newsRequest, String method) {
        ObjectNode wrappedError = mapper.createObjectNode();
        wrappedError.put("error", e.getMessage());
        wrappedError.put("method", method);
        if (newsRequest != null) {
            wrappedError.put("id", newsRequest.path("id").asText());
        }
        ObjectNode wrappedResponse = mapper.createObjectNode();
        wrappedResponse.set("errors", wrappedError);
        return wrappedResponse.toString();
    }
}