package support;

import com.typesafe.config.ConfigFactory;

/**
 * Created by idgre on 11.06.2016.
 */
public class AppConfigLoader {
    public final String ADMIN_LOGIN = ConfigFactory.load().getString("admin.login");
    public final String ADMIN_PASSWORD = ConfigFactory.load().getString("admin.password");
}
