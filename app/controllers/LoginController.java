package controllers;

import auth.Admin;
import com.google.inject.Inject;
import play.mvc.Controller;
import play.data.Form;
import play.data.FormFactory;
import play.mvc.Result;
import support.AppConfigLoader;

/**
 * Created by idgre on 11.06.2016.
 */
public class LoginController extends Controller{
    private FormFactory formFactory;
    private AppConfigLoader appConfigLoader;

    @Inject
    public LoginController(AppConfigLoader appConfigLoader, FormFactory formFactory) {
        this.appConfigLoader = appConfigLoader;
        this.formFactory = formFactory;
    }

    public Result authenticate() {
        Form<Admin> loginForm = formFactory.form(Admin.class).bindFromRequest();
        if (loginForm.hasErrors()) {
            return badRequest(views.html.login.render());
        } else {
            session().clear();
            session("login", loginForm.get().login);
            return redirect(
                    routes.NewsController.news()
            );
        }
    }

    public Result login() {
        return ok(views.html.login.render());
    }

    public Result logout() {
        session().clear();
        return redirect(routes.LoginController.login());
    }

}
