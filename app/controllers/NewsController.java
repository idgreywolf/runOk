package controllers;

import auth.Secured;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.inject.Inject;
import models.NewsModel;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.Security;
import services.interfaces.NewsService;
import support.JsonWrapper;

import java.io.IOException;

/**
 * Created by idgre on 11.06.2016.
 */
@Security.Authenticated(Secured.class)
public class NewsController extends Controller {

    private final JsonWrapper<NewsModel> wrapper;
    private final NewsService newsService;
    private final ObjectMapper mapper;
    @Inject
    public NewsController(NewsService newsService, JsonWrapper<NewsModel> wrapper, ObjectMapper mapper) {
        this.wrapper = wrapper;
        this.newsService = newsService;
        this.mapper = mapper;
    }

    public Result news() {
        return ok(views.html.news.render());
    }

    public Result post() {
        JsonNode newsRequest = request().body().asJson();
        try {
            NewsModel news = mapper.readValue(newsRequest.toString(), new TypeReference<NewsModel>() {
            });
            newsService.addNews(news);
            return ok(mapper.writeValueAsString(news));
        } catch (IOException e) {
            return ok(wrapper.wrapError(e, newsRequest, request().method()));

        }
    }

    public Result get(int page, int size) {
        try {
            return ok(wrapper.wrapResponse(newsService.get(page, size), "response", newsService.getNewsCount())).as("application/json");
        } catch (Exception e) {
            return ok(wrapper.wrapError(e, null, request().method()));
        }
    }
}
