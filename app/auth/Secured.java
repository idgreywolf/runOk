package auth;

import play.mvc.Http;
import play.mvc.Result;
import play.mvc.Security;

/**
 * Created by idgre on 11.06.2016.
 */
public class Secured extends Security.Authenticator  {
    @Override
    public String getUsername(Http.Context ctx) {
        return ctx.session().get("login");
    }

    @Override
    public Result onUnauthorized(Http.Context ctx) {
        return unauthorized(views.html.login.render());
    }
}
