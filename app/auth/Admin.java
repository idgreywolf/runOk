package auth;

import support.AppConfigLoader;

/**
 * Created by idgre on 11.06.2016.
 */
public class Admin {
    public String login;
    public String password;

    public String validate(){
        AppConfigLoader appConfigLoader= new AppConfigLoader();
        if ((appConfigLoader.ADMIN_LOGIN.equals(login))&&(appConfigLoader.ADMIN_PASSWORD.equals(password))){
            return null;
        }else {
            return "Invalid login or password!";
        }
    }
}
