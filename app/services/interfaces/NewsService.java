package services.interfaces;

import com.google.inject.ImplementedBy;
import models.NewsModel;
import services.NewsServiceImpl;

import java.util.List;

/**
 * Created by idgre on 11.06.2016.
 */
@ImplementedBy(NewsServiceImpl.class)
public interface NewsService {
    void addNews(List<NewsModel> news);

    void addNews(NewsModel news);

    NewsModel getNewsById(long id);

    List<NewsModel> getAllNews();

    List<NewsModel> get(int page, int size);

    int getNewsCount();

    void updateNews(NewsModel newsModel);

    void deleteNews(NewsModel newsModel);
}
