package services;

import models.NewsModel;
import services.interfaces.NewsService;

import java.util.Date;
import java.util.List;

/**
 * Created by idgre on 11.06.2016.
 */
public class NewsServiceImpl implements NewsService{

    @Override
    public void addNews(List<NewsModel> news) {
        for (NewsModel newsModel : news) {
            addNews(newsModel);
        }
    }

    @Override
    public void addNews(NewsModel news) {
        news.setNewsDate(new Date());
        news.save();
    }


    @Override
    public NewsModel getNewsById(long id) {
        return NewsModel.find.byId(id);
    }

    @Override
    public List<NewsModel> getAllNews() {
        return NewsModel.find
                .orderBy(NewsModel.NEWS_DATE_COLUMN + " desc")
                .findList();
    }

    @Override
    public List<NewsModel> get(int page, int size) {
        return NewsModel.find
                .orderBy(NewsModel.NEWS_DATE_COLUMN + " desc")
                .findPagedList(page - 1, size).getList();
    }

    @Override
    public int getNewsCount() {
        return NewsModel.find.findRowCount();
    }

    @Override
    public void updateNews(NewsModel newsModel) {
        newsModel.update();
    }

    @Override
    public void deleteNews(NewsModel newsModel) {
        newsModel.delete();
    }

}
