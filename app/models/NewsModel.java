package models;

import com.avaje.ebean.Model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

/**
 * Created by idgre on 05.05.2016.
 */
@Entity
@Table(name = "news")
public class NewsModel extends Model {
    public static final String IMAGE_COLUMN = "image";
    public static final String NEWS_DATE_COLUMN = "news_date";
    public static final String TITLE_COLUMN = "title";
    public static final String SUBTITLE_COLUMN = "subtitle";

    @Id
    long id;

    @Column(nullable = false, name = TITLE_COLUMN)
    String title;

    @Column(name = SUBTITLE_COLUMN)
    String subtitle;

    @Column(name = IMAGE_COLUMN)
    String image;


    @Column(name = NEWS_DATE_COLUMN)
    Date newsDate;

    public static Finder<Long, NewsModel> find = new Finder<>(NewsModel.class);

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public Date getNewsDate() {
        return newsDate;
    }

    public void setNewsDate(Date newsDate) {
        this.newsDate = newsDate;
    }

    public String getSubtitle() {
        return subtitle;
    }

    public void setSubtitle(String subtitle) {
        this.subtitle = subtitle;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
