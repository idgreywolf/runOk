var Notifications = {
    popupNotification: null,
    init: function () {
        this.popupNotification = $("#popupNotification").kendoNotification().data("kendoNotification");
    },
    showPopupNotification: function (message, type) {
        this.popupNotification.show(message, type);
    }
}