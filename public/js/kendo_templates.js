var KendoTemplates = {
    dateTemplate: function (d) {
        return d? new Date(d).toDateString(): "";
    },
    urlTemplate: function (url) {
        return url ? "<img class='news-pic' src='"+url+"')>" : "";

    }
}