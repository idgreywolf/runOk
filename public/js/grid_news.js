$(document).ready(function () {
    Datasources.init();
    News.init();
});

var Datasources = {
    newssource: null,
    init: function () {
        this.newssource = new kendo.data.DataSource({
            pageSize: KendoDS.pageSize,
            serverPaging: true,
            transport: KendoDS.buildTransport("/newsmanagement"),
            schema: {
                data: "response",
                total: "total",
                errors: "errors",
                model: {
                    id: "id",
                    fields: {
                        id: {
                            editable: false,
                            nullable: false
                        },
                        title: {
                            nullable: false, type: "string",
                            validation: {
                                required: true,
                                maxLengthValidation: function (input) {
                                    if (input.is("[name='image']") && input.val() != ""){
                                        input.attr("data-maxLengthValidation-msg", "Incorrect URL format!");
                                        // Copyright (c) 2010-2013 Diego Perini, MIT licensed
                                        // https://gist.github.com/dperini/729294
                                        // see also https://mathiasbynens.be/demo/url-regex
                                        // modified to allow protocol-relative URLs
                                        return /^(?:(?:(?:https?|ftp):)?\/\/)(?:\S+(?::\S*)?@)?(?:(?!(?:10|127)(?:\.\d{1,3}){3})(?!(?:169\.254|192\.168)(?:\.\d{1,3}){2})(?!172\.(?:1[6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)(?:\.(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)*(?:\.(?:[a-z\u00a1-\uffff]{2,})).?)(?::\d{2,5})?(?:[/?#]\S*)?$/i.test(input.val());
                                    }
                                    return true;
                                }
                            }
                        },
                        subtitle: {
                            nullable: true,
                            type: "string",
                            validation: {
                                required: false,
                            }

                        },
                        image: {
                            nullable: true,
                            type: "string",
                            validation: {
                                required: false
                            }
                        },
                        newsDate: {
                            editable: false,
                            nullable: true,
                            type: "number"
                        }
                    }
                }
            }
        });
    }
};

var News = {
    grid: null,
    wnd: null,
    template: null,
    init: function () {
        //this.wnd = $("#preview").kendoWindow({
        //    title: "Preview",
        //    modal: true,
        //    visible: false,
        //    resizable: false,
        //    width: 300
        //}).data("kendoWindow");
        //this.template = kendo.template($('#preview-template').html());
        Datasources.newssource.bind("error", this.onError);
        this.grid = $("#news_grid").kendoGrid({
            dataSource: Datasources.newssource,
            height: 550,
            toolbar: ["create"],
            sortable: true,
            editable: "popup",
            dataBound: this.onDataBound,
            pageable: {
                refresh: true,
                pageSizes: true,
                buttonCount: 5
            },
            edit: function (e) {
                e.container.find(".k-edit-label:last, .k-edit-field:last").hide();
            },
            columns: [
                {
                    field: "title",
                    title: "Title"
                }, {
                    field: "subtitle",
                    title: "Subtitle"
                },{
                    field: "image",
                    title: "Image URL",
                    template: function (data) {
                        return KendoTemplates.urlTemplate(data.image)
                    }
                }, {
                    field: "newsDate",
                    title: "News Date",
                    template: function (data) {
                        return KendoTemplates.dateTemplate(data.newsDate);
                    }
                }, {
                    command: [{
                        name: "edit",
                        text: "Edit",
                        className: "k-grid-edit"
                    }, {
                        name: "destroy",
                        text: "Delete",
                        className: "k-grid-delete"
                    }
                    ], title: "&nbsp;", width: "auto"
                }
            ]
        }).data("kendoGrid");
    },
    onError: function (e) {
        KendoDS.onError.call(this, e, News.grid);
        e.preventDefault();
        return false;
    },
    onDataBound: function (e) {
        var grid = e.sender;
        if (grid.dataSource.total() == 0) {
            var colCount = grid.columns.length;
            $(grid.wrapper).find('tbody').append('<tr class="kendo-data-row"><td colspan="' + colCount + '" class="no-data">' + 'No news uploaded.' + '</td></tr>');
        }

        if (grid.dataSource.totalPages() <= 1) {
            grid.pager.element.hide();
        }
        else {
            grid.pager.element.show();
        }
    }

};

