$(document).ready(function () {
    Notifications.init();
});

var KendoDS = {
    pageSize: 20,
    buildTransport: function (url) {
        var transport = {
            read: {
                url: url,
                type: "GET",
                dataType: "json",
                contentType: "application/json"
            },
            update: {
                url: url,
                type: "PUT",
                dataType: "json",
                contentType: "application/json"
            },
            destroy: {
                url: url,
                type: "DELETE",
                dataType: "json",
                contentType: "application/json"
            },
            create: {
                url: url,
                type: "POST",
                dataType: "json",
                contentType: "application/json"
            },
            parameterMap: function (data, type) {
                return type == "read" ? data : kendo.stringify(data);
            }
        };
        return transport;
    },

    processModel: function (dataSource, httpMethod, modelId, grid, callback) {
        switch (httpMethod) {
            case 'GET':
                break;
            case 'POST':
                if (grid) {
                    var dataItem;
                    for (var i = 0; i < dataSource.data().length; i++) {
                        if (dataSource.data()[i].id === "") {
                            dataItem = dataSource.data()[i];
                        }
                    }

                    if (typeof callback == 'function') {
                        dataSource.cancelChanges();
                        callback(dataItem);
                    }
                }
                break;
            case 'PUT':
                if (grid) {
                    var dataItem = dataSource.get(modelId);

                    if (typeof callback == 'function') {
                        callback(dataItem);
                    }
                }

                break;
            case 'DELETE':
                dataSource.cancelChanges();
                break;
        }
    },
    onError: function (e, grid, callback) {
        if (!e.errors) {
            return;
        }

        var error = e.errors.error;
        var httpMethod = e.errors.method;
        var modelId = e.errors.id;

        if (grid) {
            grid.one('dataBinding', function (e) {
                e.preventDefault();
            });
        }

        KendoDS.processModel(this, httpMethod, modelId, grid, callback);

        if (error) {
            Notifications.showPopupNotification(error, "error");
        }
    }
};