$(document).ready (function(){
    var newsBlock = $("#newsCarousel .active .news-block");
   wrapNews(newsBlock);
    $('#newsCarousel').on('slid.bs.carousel', function (e) {
        var newsBlock = $(e.relatedTarget).find(".news-block");
        wrapNews(newsBlock);
        $(document).scrollTop( $('#news').offset().top );
    })
    collapseNavbar();
    $(window).scroll(collapseNavbar);
});

function wrapNews(newsBlock) {
    if (newsBlock.height() > 600) {
        newsBlock.height(600);
        var btnDown = $('<div>').addClass("btn btn-primary").append($('<div>').attr("aria-hidden","true").html("ЧИТАТИ ДАЛІ"));
        newsBlock.append($('<div>').addClass("fadeout").append(btnDown));
        btnDown.on("click", function(){
            newsBlock.height("auto");
            newsBlock.find(".fadeout, .btn").remove();
        });
    } else {
        newsBlock.height(600);
    }
}

function collapseNavbar() {
    if ($(".navbar").offset().top > 50) {
        $(".navbar-fixed-top").addClass("top-nav-collapse");
    } else {
        $(".navbar-fixed-top").removeClass("top-nav-collapse");
    }
}

// jQuery for page scrolling feature - requires jQuery Easing plugin
$(function() {
    $('a.page-scroll').bind('click', function(event) {
        var $anchor = $(this);
        $('html, body').stop().animate({
            scrollTop: $($anchor.attr('href')).offset().top
        }, 1500, 'easeInOutExpo');
        event.preventDefault();
    });
});

// Closes the Responsive Menu on Menu Item Click
$('.navbar-collapse ul li a').click(function() {
  if ($(this).attr('class') != 'dropdown-toggle active' && $(this).attr('class') != 'dropdown-toggle') {
    $('.navbar-toggle:visible').click();
  }
});


